package ru.ekfedorov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.endpoint.SessionDTO;
import ru.ekfedorov.tm.exception.system.NullObjectException;

public final class ProjectClearAllCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Clear all project.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-clear";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("[CLEAR]");
        endpointLocator.getAdminEndpoint().clearProject(session);
        System.out.println("--- successfully cleared ---\n");
    }

}
