package ru.ekfedorov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.endpoint.Project;
import ru.ekfedorov.tm.endpoint.ProjectDTO;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.endpoint.SessionDTO;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.util.TerminalUtil;

public final class ProjectShowByNameCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show project by name.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-view-by-name";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = endpointLocator.getProjectEndpoint().findProjectOneByName(session, name);
        if (project == null) throw new NullProjectException();
        showProject(project);
        System.out.println();
    }

}
