package ru.ekfedorov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.endpoint.TaskDTO;
import ru.ekfedorov.tm.exception.system.NullTaskException;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(@Nullable final TaskDTO task) throws Exception {
        if (task == null) throw new NullTaskException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().value());
        System.out.println("PROJECT: " + task.getProjectId());
    }

}
