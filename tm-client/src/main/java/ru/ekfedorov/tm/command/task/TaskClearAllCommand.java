package ru.ekfedorov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.endpoint.SessionDTO;
import ru.ekfedorov.tm.exception.system.NullObjectException;

public final class TaskClearAllCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Clear all task.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-clear";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("[CLEAR]");
        endpointLocator.getAdminEndpoint().clearTask(session);
        System.out.println("--- successfully cleared ---\n");
    }

}
