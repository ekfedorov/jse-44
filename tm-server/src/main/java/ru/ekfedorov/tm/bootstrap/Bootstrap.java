package ru.ekfedorov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.*;
import ru.ekfedorov.tm.api.service.*;
import ru.ekfedorov.tm.api.service.dto.*;
import ru.ekfedorov.tm.api.service.model.*;
import ru.ekfedorov.tm.endpoint.*;
import ru.ekfedorov.tm.dto.SessionDTO;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.service.*;
import ru.ekfedorov.tm.service.dto.*;
import ru.ekfedorov.tm.service.model.*;
import ru.ekfedorov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
@NoArgsConstructor
public class Bootstrap implements ServiceLocator {

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    public final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    public final ILoggerService loggerService = new LoggerService();

    @NotNull
    public final IProjectDTOService projectDTOService = new ProjectDTOService(connectionService);

    @NotNull
    public final IProjectTaskDTOService projectTaskDTOService = new ProjectTaskDTOService(connectionService);

    @NotNull
    public final ISessionDTOService sessionDTOService = new SessionDTOService(connectionService, this);

    @NotNull
    public final ITaskDTOService taskDTOService = new TaskDTOService(connectionService);

    @NotNull
    public final IUserDTOService userDTOService = new UserDTOService(propertyService, connectionService);

    @NotNull
    public final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    public final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    public final ISessionService sessionService = new SessionService(connectionService, this);

    @NotNull
    public final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    public final IUserService userService = new UserService(propertyService, connectionService);

    @NotNull
    public final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    public final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    public final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    public final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    public final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    public final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @Nullable
    private SessionDTO session = null;

    public void init() {
        initPID();
        initEndpoint();
        initUser();
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(userEndpoint);
        registry(adminUserEndpoint);
        registry(adminEndpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initUser() {
        if (!userService.findByLogin("test").isPresent()) {
            userService.create("test", "test", "test@test.ru");
        }
        if (!userService.findByLogin("test2").isPresent()) {
            userService.create("test2", "test", "test@test.ru");
        }
        if (!userService.findByLogin("admin").isPresent()) {
            userService.create("admin", "admin", Role.ADMIN);
        }
    }

}
