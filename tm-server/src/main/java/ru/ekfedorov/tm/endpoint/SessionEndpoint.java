package ru.ekfedorov.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.ISessionEndpoint;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.dto.SessionDTO;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.UserIsLockedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws UserIsLockedException, AccessDeniedException {
        return serviceLocator.getSessionDTOService().open(login, password);
    }

    @Override
    @Nullable
    @WebMethod
    public SessionDTO closeSession(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AccessDeniedException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getSessionDTOService().close(session);
    }

}
