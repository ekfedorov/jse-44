package ru.ekfedorov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.service.dto.*;
import ru.ekfedorov.tm.api.service.model.*;

public interface ServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IProjectDTOService getProjectDTOService();

    @NotNull
    IProjectTaskDTOService getProjectTaskDTOService();

    @NotNull
    ISessionDTOService getSessionDTOService();

    @NotNull
    ITaskDTOService getTaskDTOService();

    @NotNull
    IUserDTOService getUserDTOService();

}
