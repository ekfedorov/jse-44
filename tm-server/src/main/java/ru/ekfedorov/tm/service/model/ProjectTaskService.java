package ru.ekfedorov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.model.IProjectRepository;
import ru.ekfedorov.tm.api.repository.model.ITaskRepository;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.model.IProjectTaskService;
import ru.ekfedorov.tm.exception.empty.ProjectIdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.TaskIdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.UserIdIsEmptyException;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.repository.model.ProjectRepository;
import ru.ekfedorov.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @SneakyThrows
    @Override
    public void bindTaskByProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        if (isEmpty(taskId)) throw new TaskIdIsEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.bindTaskByProjectId(userId, projectId, taskId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @SneakyThrows
    @Override
    public void removeProjectById(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeOneByIdAndUserId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId, @Nullable final String taskId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(taskId)) throw new TaskIdIsEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.unbindTaskFromProjectId(userId, taskId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
