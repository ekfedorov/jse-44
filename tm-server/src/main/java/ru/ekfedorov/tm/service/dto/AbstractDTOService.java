package ru.ekfedorov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.service.dto.IServiceDTO;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.dto.AbstractEntityDTO;


public abstract class AbstractDTOService<E extends AbstractEntityDTO> implements IServiceDTO<E> {

    @NotNull
    public final IConnectionService connectionService;

    public AbstractDTOService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
