package ru.ekfedorov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.entity.IWBS;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "app_project")
@NoArgsConstructor
public class ProjectDTO extends AbstractBusinessEntityDTO implements IWBS {

    public ProjectDTO(@Nullable final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

}
